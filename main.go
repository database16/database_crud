package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "my_user3"
	password = "my_user3"
	dbname   = "my_db3"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	CheckError(err)

	defer db.Close()
	fmt.Println("Muvaffaqiyatli ulandi: !")

	var (
		batalyon, jangovar_mashinalar string
		xodimlar_soni                 int
	)

	//createTableFunc(db)

	//insertTableFunc(db)

	//updateFunc(db)

	deleteFunc(db)

	boshqatdan := true
	var tanlov int
	for boshqatdan {
		fmt.Printf("1.Malumot kiritish \n2.Malumotlarni o'zgartirish\n3.Malumot o'chirish\n")
		fmt.Printf("Tanlovingizni kiriting: ")
		fmt.Scanf("%d", &tanlov)
		if tanlov > 0 && tanlov < 4 {
			boshqatdan = false
		}

	}

	switch tanlov {
	case 1:
		fmt.Println("Malumot kiritishni tanladingiz: ")
		fmt.Println("Batalyon: ")
		fmt.Scanf("%s", &batalyon)
		fmt.Println("Jangovar_mashinalar: ")
		fmt.Scanf("%s", &jangovar_mashinalar)
		fmt.Println("Xodimlar soni: ")
		fmt.Scanf("%d", &xodimlar_soni)
		insertTableByParam(db, batalyon, jangovar_mashinalar, xodimlar_soni)

	case 2:
		v := ""
		n := ""
		fmt.Println("Malumot o'zgartirishni tanladingiz tanladingiz: ")
		fmt.Println("Eski ismni kiriting")
		fmt.Scanf("%s", &v)
		fmt.Println("Yangi ismni kiririting")
		fmt.Scanf("%s", &n)
		updateByParam(db, n, v)

	case 3:
		var i int
		fmt.Println("Malumot o'chirishni tanladingiz")
		fmt.Println("delete kiriting")
		fmt.Scanf("%d", &i)
		deleteFuncByParam(db, i)
	}

}

func createTableFunc(db *sql.DB) {
	createTable := `create table harbiy_qism(Batalyon varchar, Jangovor_mashinalar varchar, xodimlar_soni integer)`
	_, err := db.Exec(createTable)
	CheckError(err)
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
func insertTableFunc(db *sql.DB) {
	insertDb := `insert into harbiy_qism(batalyon,jangovor_mashinalar,xodimlar_soni)values('BSO','KUKE',123)`
	_, err := db.Exec(insertDb)
	CheckError(err)
}

func insertTableByParam(db *sql.DB, b, j string, xodim int) {

	insertDb := `insert into harbiy_qism(batalyon,jangovor_mashinalar,xodimlar_soni) values($1,$2,$3)`
	_, err := db.Exec(insertDb, b, j, xodim)
	CheckError(err)

}

func updateFunc(db *sql.DB) {
	updateDb := `update harbiy_qism SET Batalyon = Xodimlar_soni WHERE Batalyon = jangovor_mashinalar`
	_, err := db.Exec(updateDb)
	CheckError(err)
}
func updateByParam(db *sql.DB, n, v string) {
	updateDb := `update harbiy_qism SET Xodimlar_soni = $1 WHERE Xodimlar_soni = $2`
	_, err := db.Exec(updateDb, n, v)
	CheckError(err)
}

func deleteFunc(db *sql.DB) {
	deleteDb := `delete from harbiy_qism where id = xodimlar_soni `
	_, err := db.Exec(deleteDb)
	CheckError(err)
}
func deleteFuncByParam(db *sql.DB, i int) {
	deleteDb := `delete from harbiy_qism where id = $1 `
	_, err := db.Exec(deleteDb, i)
	CheckError(err)
}
